var fib = function (number) {
    if (number < 0) {
        return 0;
    } else if (number <= 2) {
        return 1;
    } else {
        return fib(number - 1) + fib(number - 2);
    }
}
console.log("Fibannoci of 30 is", fib(30));
console.log("Fibannoci of -10 is", fib(-10));

// var localFib = function (number) {
//     if (number < 0) {
//         return 0;
//     } else if (number <= 2) {
//         return 1;
//     } else {
//         return localFib(number - 1) + localFib(number - 2);
//     }
// }
//
// var ab = function(number, callback){
//     callback(localFib(number));
// }
// module.exports = {
//     findFibannoci: ab
// };